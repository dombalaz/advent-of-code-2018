#include <algorithm>
#include <fstream>
#include <iostream>
#include <set>
#include <vector>

void task1(std::fstream &fs) {
    std::string line;
    int two = 0, three = 0;
    while(std::getline(fs, line)) {
        std::set<char> foundedChars;
        bool bTwo = false, bThree = false;
        for(const char &it : line) {
            if(foundedChars.count(it)) {
                continue;
            }
            int count = static_cast<int>(std::count(line.begin(), line.end(), it));
            if(count == 2 && !bTwo) {
                ++two;
                foundedChars.insert(it);
                bTwo = true;
            } else if(count == 3 && !bThree) {
                ++three;
                foundedChars.insert(it);
                bThree = true;
            }
        }
    }
    std::cout << two * three << std::endl;

    fs.clear();
    fs.seekg(0, std::fstream::beg);
}

void task2(std::fstream &fs) {
    std::vector<std::string> vec;
    std::string line;
    while(std::getline(fs, line)) {
        vec.push_back(line);
    }
    fs.close();
    for(std::vector<std::string>::const_iterator it = vec.begin(); it != vec.end(); ++it) {
        for(std::vector<std::string>::const_iterator it2 = it + 1; it2 != vec.end(); ++it2) {
            int differ = 0;
            int position = 0;
            int i = 0;
            for(std::string::const_iterator it3 = (*it).begin(), it4 = (*it2).begin();
                it3 != (*it).end() && it4 != (*it2).end(); ++it3, ++it4, ++i) {
                if(*it3 != *it4) {
                    ++differ;
                    position = i;
                    if(differ == 2) {
                        break;
                    }
                }
            }
            if(differ == 1) {
                std::string tmp = *it;
                std::cout << tmp.erase(static_cast<size_t>(position), 1) << std::endl;
                return;
            }
        }
    }
}


int main(int argc, char **argv) {
    if(argc != 2) {
        std::cerr << "Bad arguments" << std::endl;
        return 1;
    }
    std::fstream fs;
    fs.open(argv[1], std::fstream::in);
    if(!fs.is_open()) {
        std::cerr << "Failed to open file" << std::endl;
        return 2;
    }
    task1(fs);
    task2(fs);
    fs.close();
    return 0;
}
