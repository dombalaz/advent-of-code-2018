#include <fstream>
#include <iostream>
#include <set>

const int startingPosition = 0;

void task1(std::fstream &fs) {
    int result = startingPosition;
    std::string line;

    while(std::getline(fs, line)) {
        result += stoi(line);
    }

    std::cout << result << std::endl;
}

void task2(std::fstream &fs) {
    int result = 0;
    std::string line;
    std::set<int> numbers;

    while(true) {
        fs.clear();
        fs.seekg(0, std::fstream::beg);

        while(std::getline(fs, line)) {
            result += stoi(line);
            if(numbers.count(result)) {
                std::cout << result << std::endl;
                return;
            } else {
                numbers.insert(result);
            }
        }
    }
}

int main(int argc, char **argv) {
    if(argc != 2) {
        std::cerr << "Bad arguments" << std::endl;
        return 1;
    }
    std::fstream fs;
    fs.open(argv[1], std::fstream::in);
    if(!fs.is_open()) {
        std::cerr << "Failed to open file" << std::endl;
        return 2;
    }
    task1(fs);
    task2(fs);
    fs.close();
    return 0;
}

