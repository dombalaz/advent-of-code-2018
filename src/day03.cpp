#include <fstream>
#include <iostream>
#include <vector>

void task1(std::fstream &fs, std::vector<std::vector<size_t>> &vec) {
    std::string line;
    while(std::getline(fs, line)) {
        std::string token = line.substr(0, line.find(" "));
        line = line.substr(line.find(" ") + 1);
        line = line.substr(line.find(" ") + 1);

        token = line.substr(0, line.find(","));
        line = line.substr(line.find(",") + 1);
        int x = stoi(token);

        token = line.substr(0, line.find(":"));
        line = line.substr(line.find(":") + 2);
        int y = stoi(token);

        int a = stoi(line.substr(0, line.find("x")));
        int b = stoi(line.substr(line.find("x") + 1));

        for(std::vector<std::vector<size_t>>::iterator it = vec.begin() + y; it != vec.begin() + y + b && it != vec.end(); ++it) {
            for(std::vector<size_t>::iterator it2 = (*it).begin() + x; it2 != (*it).begin() + x + a && it2 != (*it).end(); ++it2) {
                ++(*it2);
            }
        }
    }

    int count = 0;
    for(std::vector<std::vector<size_t>>::const_iterator it = vec.begin(); it != vec.end(); ++it) {
        for(std::vector<size_t>::const_iterator it2 = (*it).begin(); it2 != (*it).end(); ++it2) {
            if(*it2 > 1) {
                ++count;
            }
        }
    }
    std::cout << count << std::endl;

    fs.clear();
    fs.seekg(0, std::fstream::beg);
}

void task2(std::fstream &fs, const std::vector<std::vector<size_t>> &vec) {
    std::string line;
    int id = 0;
    while(std::getline(fs, line)) {
        bool founded = true;
        std::string token = line.substr(0, line.find(" "));
        id = stoi(token.substr(1));
        line = line.substr(line.find(" ") + 1);
        line = line.substr(line.find(" ") + 1);

        token = line.substr(0, line.find(","));
        line = line.substr(line.find(",") + 1);
        size_t x = stoul(token);

        token = line.substr(0, line.find(":"));
        line = line.substr(line.find(":") + 2);
        size_t y = stoul(token);

        size_t a = stoul(line.substr(0, line.find("x")));
        size_t b = stoul(line.substr(line.find("x") + 1));

        for(size_t i = y; i < y + b; ++i) {
            for(size_t j = x; j < x + a; ++j) {
                if(vec.at(i).at(j) != 1) {
                    founded = false;
                    break;
                }
            }
            if(founded) {
                break;
            }
        }
        if(founded) {
            break;
        }
    }

    std::cout << id << std::endl;
}

int main(int argc, char **argv) {
    if(argc != 2) {
        std::cerr << "Bad arguments" << std::endl;
        return 1;
    }
    std::fstream fs;
    fs.open(argv[1], std::fstream::in);
    if(!fs.is_open()) {
        std::cerr << "Failed to open file" << std::endl;
        return 2;
    }
    std::vector<std::vector<size_t>> vec(1000, std::vector<size_t>(1000, 0));
    task1(fs, vec);
    task2(fs, vec);
    fs.close();
    return 0;
}
