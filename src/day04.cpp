#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <numeric>
#include <vector>

int compare(const std::string &lhs, const std::string &rhs) {
    std::string tmp1, tmp2;
    tmp1 = lhs.substr(1, lhs.find("]") - 1);
    tmp2 = rhs.substr(1, rhs.find("]") - 1);
    int num1, num2;

    num1 = std::stoi(tmp1.substr(0, tmp1.find("-")));
    num2 = std::stoi(tmp2.substr(0, tmp2.find("-")));
    tmp1 = tmp1.substr(tmp1.find("-") + 1);
    tmp2 = tmp2.substr(tmp2.find("-") + 1);
    if(num1 != num2) {
        return num1 < num2;
    }

    num1 = std::stoi(tmp1.substr(0, tmp1.find("-")));
    num2 = std::stoi(tmp2.substr(0, tmp2.find("-")));
    tmp1 = tmp1.substr(tmp1.find("-") + 1);
    tmp2 = tmp2.substr(tmp2.find("-") + 1);
    if(num1 != num2) {
        return num1 < num2;
    }

    num1 = std::stoi(tmp1.substr(0, tmp1.find(" ")));
    num2 = std::stoi(tmp2.substr(0, tmp2.find(" ")));
    tmp1 = tmp1.substr(tmp1.find(" ") + 1);
    tmp2 = tmp2.substr(tmp2.find(" ") + 1);
    if(num1 != num2) {
        return num1 < num2;
    }

    num1 = std::stoi(tmp1.substr(0, tmp1.find(":")));
    num2 = std::stoi(tmp2.substr(0, tmp2.find(":")));
    tmp1 = tmp1.substr(tmp1.find(":") + 1);
    tmp2 = tmp2.substr(tmp2.find(":") + 1);
    if(num1 != num2) {
        return num1 < num2;
    }

    num1 = std::stoi(tmp1);
    num2 = std::stoi(tmp2);
    if(num1 != num2) {
        return num1 < num2;
    } else {
        return num1 == num2;
    }
}

void task1(const std::vector<std::string> &vec, std::map<int, std::vector<int>> &map) {
    int guard = 0, startMinutes = 0, minutes = 0, hours = 0;
    for(auto it = vec.cbegin(); it != vec.cend(); ++it) {
        if((*it).find("#") != std::string::npos) {
            guard = stoi((*it).substr((*it).find("#") + 1, (*it).find(" ") - 1));
            hours = stoi((*it).substr((*it).find(" ") + 1, (*it).find(":") - 1));
            startMinutes = (hours == 0 ? stoi((*it).substr((*it).find(":") + 1, (*it).find("]") - 1)) : 0);

            if(!map.count(guard)) {
                map.insert(std::pair<int, std::vector<int>>(guard, std::vector<int>(60, 0)));
            }
        } else {
            minutes = stoi((*it).substr((*it).find(":") + 1, (*it).find("]") - 1));
            if((*it).find("falls") != std::string::npos) {
                startMinutes = minutes;
            } else {
                for(int i = startMinutes; i < minutes; ++i) {
                    ++map[guard][static_cast<size_t>(i)];
                }
            }
        }
    }

    auto max = std::max_element(map.begin(), map.end(), [](const std::pair<int, std::vector<int>> &lhs, const std::pair<int, std::vector<int>> &rhs) {
        return std::accumulate(lhs.second.begin(), lhs.second.end(), 0) < std::accumulate(rhs.second.begin(), rhs.second.end(), 0);
    });

    auto maxMinute = std::max_element((*max).second.begin(), (*max).second.end());

    std::cout << (*max).first * (maxMinute - (*max).second.begin()) << std::endl;
}

void task2(std::map<int, std::vector<int>> &map) {
    auto max = std::max_element(map.begin(), map.end(), [](const std::pair<int, std::vector<int>> &lhs, const std::pair<int, std::vector<int>> &rhs) {
        return (*std::max_element(lhs.second.begin(), lhs.second.end())) < (*std::max_element(rhs.second.begin(), rhs.second.end()));
    });
    auto maxMinute = std::max_element((*max).second.begin(), (*max).second.end());
    std::cout << (*max).first * (maxMinute - (*max).second.begin()) << std::endl;
}

int main(int argc, char **argv) {
    if(argc != 2) {
        std::cerr << "Bad arguments" << std::endl;
        return 1;
    }
    std::fstream fs;
    fs.open(argv[1], std::fstream::in);
    if(!fs.is_open()) {
        std::cerr << "Failed to open file" << std::endl;
        return 2;
    }
    std::vector<std::string> vec;
    std::string line;
    while(std::getline(fs, line)) {
        vec.push_back(line);
    }
    fs.close();

    std::sort(vec.begin(), vec.end(), &compare);

    std::map<int, std::vector<int>> map;

    task1(vec, map);
    task2(map);
    return 0;
}
